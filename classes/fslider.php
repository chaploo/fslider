<?php

/**
 * @module          FSlider
 * @author          ISB
 * @copyright       2019-2020 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class fslider extends LEPTON_abstract
{
	public $all_fsliders = array();
	public $database = 0;
	public $admin = 0;
	public $addon_color = 'olive';
	public $action = ADMIN_URL.'/pages/modify.php?page_id=';
	public $mod_action = LEPTON_URL.'/modules/fslider/modify.php';	
		
	
	public static $instance;	
	
	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();		
		$this->admin = LEPTON_admin::getInstance();	
	}
	
	public function init_section( $iPageID = 0, $iSectionID = 0 )
	{
		//get array of all_fsliders
		$this->all_fsliders = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_fslider WHERE page_id=". $iPageID." AND section_id=". $iSectionID." ORDER BY position ASC",
			true,
			$this->all_fsliders,
			true
		);		
	}

	public function show_info()
	{
		$support_link = '<a href="#">NO Live-Support / FAQ</a>';	
		$readme_link = '<a href="https://www.internet-service-berlin.de/webdesign/module/fslider.php" class="extern" target="_blank">Readme</a>';
		$page_id = $_POST['show_info'];
		
		
//die(LEPTON_tools::display($page_id, 'pre','ui blue message'));
		
		$form_values = array(
			'image'			=> "https://www.internet-service-berlin.de/media/module/fslider.gif",// 201x201px
			'oFS'			=> $this,
			'readme_link'	=> $readme_link,
			'page_id'		=> $page_id,
			'SUPPORT'		=> $support_link,	
			'leptoken'		=> get_leptoken()
			
		);

		/**	
		 *	get the template-engine.
		 */
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('fslider');
			
		echo $oTwig->render( 
			"@fslider/info.lte",	//	template-filename
			$form_values			//	template-data
		);	
		exit();		
	}	

	public function edit_fslider( $ifsliderId = 0 )
	{
		if(isset($_POST['edit_fslider'] ) && $_POST['edit_fslider'] != 0 )
		{	
			$fslider_id = intval( $_POST['edit_fslider']);
			
			// get current fslider from database
			$current_fslider = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_fslider WHERE id = ".$fslider_id." ",
				true,
				$current_fslider,
				false
			);	

			//create wysiwyg : show_wysiwyg_editor($name,$id,$content,$width,$height, $prompt)
			require(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
			$show_wysiwyg = show_wysiwyg_editor('content','fs_content',$current_fslider['content'],'100%','250',false);				
		
			$form_values = array(
				'oFS'			=> $this,
				'current_fslider'	=> $current_fslider,
				'show_wysiwyg'	=> $show_wysiwyg,				
				'leptoken'		=> get_leptoken()
				
			);

			/**	
			 *	get the template-engine.
			 */
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('fslider');
				
			echo $oTwig->render( 
				"@fslider/fslider_detail.lte",	//	template-filename
				$form_values			//	template-data
			);		


		

			exit();
		}
	
	} 
	

	public function save_fslider( $ifsliderId = 0 )
	{
		
		if(isset($_POST['save_fslider'] ) && $_POST['save_fslider'] != 0 )
		{
			$fslider_id = intval($_POST['save_fslider']);

			if(!isset($_POST['active']))
			{
				$_POST['active'] = 0;
			}
			else
			{
				$_POST['active'] = $_POST['active'];
			}
			
			$request = LEPTON_request::getInstance();	
		
			$all_names = array (
				'title'			=> array ('type' => 'str', 'default' => "", 'range' =>""),
				'image'			=> array ('type' => 'str', 'default' => "", 'range' =>""),
				'link'			=> array ('type' => 'str', 'default' => "", 'range' =>""),
				'content'		=> array ('type' => 'str', 'default' => "", 'range' =>""),
				'button'		=> array ('type' => 'str', 'default' => "", 'range' =>""),
				'active'		=> array ('type' => 'int', 'default' => "1", 'range' =>"")
			);		

			$all_values = $request->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_fslider";
			$result = $this->database->build_and_execute( 'UPDATE', $table, $all_values,'id = '.$fslider_id.' ');	

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}
			else 
			{
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			}				
			
			
		}
	}
	
	public function duplicate_fslider( $ifsliderId = 0 )
	{
		if(isset($_POST['duplicate_fslider'] ) && $_POST['duplicate_fslider'] != 0 )
		{
			$fslider_id = intval($_POST['duplicate_fslider']);

			// get current fslider from database
			$current_fslider = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_fslider WHERE id = ".$fslider_id." ",
				true,
				$current_fslider,
				false
			);	
			
			
			// Get new order
			$order = new LEPTON_order(TABLE_PREFIX.'mod_fslider', 'position', 'id', 'section_id');
			$position = $order->get_new($current_fslider['section_id']);			

			$fields = array(
				'id' => NULL,
				'page_id' => $current_fslider['page_id'],
				'section_id' => $current_fslider['section_id'],
				'title' => $current_fslider['title']."-Kopie",
				'content' => $current_fslider['content'],
				'position' => $position
			);
			
			$result = $this->database->build_and_execute (
				"INSERT",
				TABLE_PREFIX."mod_fslider",
				$fields
			);

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}
			else 
			{
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			}				
			
			
		}
	}

	public function move_down( $ifsliderId = 0 )
	{
		if(isset($_POST['move_down'] ) && $_POST['move_down'] != 0 )
		{
			$fslider_id = intval($_POST['move_down']);
			$section_id = $this->database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_fslider WHERE id = ".$fslider_id." ");	
		
			// Create new order object an reorder
			$order = new LEPTON_order(TABLE_PREFIX.'mod_fslider', 'position', 'id', 'section_id');
			if($order->move_down($fslider_id)) 
			{
				// Clean up ordering
				$order = new LEPTON_order(TABLE_PREFIX.'mod_fslider', 'position', 'id', 'section_id');
				$order->clean($section_id); 
				
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			} else 
			{
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}			
			
		}
	}	


	public function move_up( $ifsliderId = 0 )
	{
		if(isset($_POST['move_up'] ) && $_POST['move_up'] != 0 )
		{
			$fslider_id = intval($_POST['move_up']);
			$section_id = $this->database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_fslider WHERE id = ".$fslider_id." ");	
		
			// Create new order object an reorder
			$order = new LEPTON_order(TABLE_PREFIX.'mod_fslider', 'position', 'id', 'section_id');
			if($order->move_up($fslider_id)) 
			{
				// Clean up ordering
				$order = new LEPTON_order(TABLE_PREFIX.'mod_fslider', 'position', 'id', 'section_id');
				$order->clean($section_id); 
				
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			} else 
			{
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}			
			
		}
	}	
	
	
	public function delete_fslider( $ifsliderId = 0 )
	{
		if(isset($_POST['delete_fslider'] ) && $_POST['delete_fslider'] != 0 )
		{
			$fslider_id = intval($_POST['delete_fslider']);
			$section_id = $this->database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_fslider WHERE id = ".$fslider_id." ");	

			$result = $this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_fslider WHERE id = ".$fslider_id." ");	

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				exit(0);
			}
			else 
			{
				// Clean up ordering
				$order = new LEPTON_order(TABLE_PREFIX.'mod_fslider', 'position', 'id', 'section_id');
				$order->clean($section_id); 
				
				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['delete_ok'],'pre','ui green message'));
				header("Refresh:1; url=".$this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']."");
				exit(0);
			}				
			
			
		}
	}
	
}
