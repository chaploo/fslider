<?php

/**
 * @module          FSlider
 * @author          ISB
 * @copyright       2019-2020 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class fslider_frontend extends LEPTON_abstract
{
	public $database = 0;
	public $all_fsliders = array();
	public $action_url = LEPTON_URL.'/modules/fslider/';	
	public $view_url = LEPTON_URL.PAGES_DIRECTORY;
		
	
	public static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->init_page();
	}
	
	public function init_page( $iPageID = 0, $iSectionID = 0 )
	{
		$page_link = $this->database->get_one("SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id=". $iPageID."");
		$this->view_url = LEPTON_URL.PAGES_DIRECTORY.$page_link.PAGE_EXTENSION;
		
		//get array of all_fsliders
		$this->all_fsliders = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_fslider WHERE section_id=". $iSectionID." ORDER BY position ASC ",
			true,
			$this->all_fsliders,
			true
		);			
	}		
}
