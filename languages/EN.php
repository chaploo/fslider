<?php

/**
 * @module          FSlider
 * @author          ISB
 * @copyright       2019-2020 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_FSLIDER = array(
	'action'	    => "Aktion",
	'content'	    => "Inhalt",
	'delete_ok'     => "Datensatz erfolgreich gelöscht",
	'duplicate'     => "Kopieren",
	'edit'	        => "Bearbeiten",
	'edit_css'	    => "CSS bearbeiten",
	'info'	        => "Addon Info",
	'header1'	    => "ID",
	'header2'	    => "Titel",
	'header3'	    => "Aktiv",
	'header4'	    => "Bearbeiten",
	'header5'	    => "Kopieren",
	'header6'	    => "Löschen",
	'header7'	    => "Weiterlesen-Text",
	'header8'	    => "Bild",
	'header9'	    => "Bild-Verlinkung",
	'list_header1'	=> "Hinzufügen/Ändern Slider-Bild",
	'save_ok'	    => "Daten erfolgreich gespeichert",
	'to_delete'	    => "wirklich löschen",
	'want_delete'	=> "Wollen Sie den Datensatz",
);
?>