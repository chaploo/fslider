### FSlider
=========

Simple fading slideshow for LEPTON CMS


#### Requirements

* [LEPTON CMS][1], Version see precheck.php

#### Installation

* download latest [fslider.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon create a page or section using FSlider module. <br />
For further details please see [readme file][3].


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/modules/fslider.php
[3]: https://www.internet-service-berlin.de/webdesign/module/fslider.php

