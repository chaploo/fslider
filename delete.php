<?php

/**
 * @module          FSlider
 * @author          ISB
 * @copyright       2019-2020 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php


//get instance of own module class

$table = TABLE_PREFIX."mod_fslider";
$data = array();
$database->execute_query(
	"SELECT * from ". $table ." where section_id = ".$section_id." ",
	true,
	$data,
	true
);	

// echo (LEPTON_tools::display($data,'pre','ui message'));

if (count($data) > 0 ) {
	// delete fslider from table
	$database->simple_query("DELETE from ".$table." where section_id = ".$section_id);
}

?>