<?php

/**
 * @module          FSlider
 * @author          ISB
 * @copyright       2019-2020 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

//echo(LEPTON_tools::display($_POST,'pre','ui message'));
//get instance of own module class
$oACF = accordion_frontend::getInstance();
$oACF->init_page( $page_id, $section_id);
if(empty($_POST))  
{
	
	// marker settings
	$form_values = array(
		'oACF'			=> $oACF,
		'section_id'	=> $section_id,
		'page_id'		=> $page_id,	
		'leptoken'		=> get_leptoken()
	);

	/**	
	 *	get the template-engine.
	 */
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('accordion');
		
	echo $oTwig->render( 
		"@accordion/view.lte",	//	template-filename
		$form_values				//	template-data
	);

}

?>