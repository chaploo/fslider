<?php

/**
 * @module          FSlider
 * @author          ISB
 * @copyright       2019-2020 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_directory     = 'fslider';
$module_name          = 'FSlider';
$module_function      = 'page';
$module_version       = '1.0.0';
$module_platform      = '4.x';
$module_author        = 'ISB';
$module_home          = 'https://www.internet-service-berlin.de';
$module_guid          = '9af45668-a074-4565-b928-1faf99251d56';
$module_license       = '<a href="https://www.gnu.org/licenses/gpl-3.0" target="_blank">GNU General Public License 3</a>';
$module_license_terms = 'no license terms';
$module_description   = 'Displays a fading slideshow';


?>