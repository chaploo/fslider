<?php

/**
 * @module          FSlider
 * @author          ISB
 * @copyright       2019-2020 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


$debug = true;

if (true === $debug) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}

//get instance of own module class
$oFS = fslider::getInstance();
$oFS->init_section( $page_id, $section_id );

//die(LEPTON_tools::display($_POST,'pre','ui message'));


if(isset($_POST['show_info'] ))
{
	$oFS->show_info();
} 
elseif(isset($_POST['edit_fslider'] ) && $_POST['edit_fslider'] != 0 )
{
		$oFS->edit_fslider($_POST['edit_fslider']);
}
elseif(isset($_POST['save_fslider'] ) && $_POST['save_fslider'] != 0 )
{
		$oFS->save_fslider($_POST['save_fslider']);
}
elseif(isset($_POST['duplicate_fslider'] ) && $_POST['duplicate_fslider'] != 0 )
{
		$oFS->duplicate_fslider($_POST['duplicate_fslider']);
}
elseif(isset($_POST['delete_fslider'] ) && $_POST['delete_fslider'] != 0 )
{
		$oFS->delete_fslider($_POST['delete_fslider']);
}
elseif(isset($_POST['move_down'] ) && $_POST['move_down'] != 0 )
{
		$oFS->move_down($_POST['move_down']);
}
elseif(isset($_POST['move_up'] ) && $_POST['move_up'] != 0 )
{
		$oFS->move_up($_POST['move_up']);
}
else
{	
	$min_position = $oFS->database->get_one(" SELECT MIN(position) FROM ".TABLE_PREFIX."mod_fslider WHERE section_id = ".$section_id." ");
	$max_position =	$oFS->database->get_one(" SELECT MAX(position) FROM ".TABLE_PREFIX."mod_fslider WHERE section_id = ".$section_id." ");

	// Include core functions to edit the optional module CSS files (frontend.css, backend.css)
	require_once(LEPTON_PATH.'/framework/summary.module_edit_css.php');	
	
	ob_start();
	edit_module_css('fslider');
	$call_edit_module_css = ob_get_clean();	
	
	$form_values = array(
		'min_pos'		=> $min_position,
		'max_pos'		=> $max_position,
		'oFS'			=> $oFS,
		'section_id'	=> $section_id,
		'page_id'		=> $page_id,
		'read_me'		=> 'https://www.internet-service-berlin.de/webdesign/module/fslider.php',
		'module_css'	=> $call_edit_module_css,
		'leptoken'		=> get_leptoken()
		
	);
	
	 //	get the template-engine.
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('fslider');
		
	echo $oTwig->render( 
		"@fslider/modify.lte",	//	template-filename
		$form_values			//	template-data
	);
}
?>